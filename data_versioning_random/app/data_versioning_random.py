MOUNT_PATH = None
import os
import sys
import types
import pickle
import urllib
import marshal
import sklearn
import tarfile
import warnings
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer
from pandas.plotting import scatter_matrix
from sklearn.compose import ColumnTransformer
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
from sklearn.model_selection import StratifiedShuffleSplit
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot

PICKLE_PATH = "/mnt/nfs/data/jupyter_experiments/projects/testing_house_5/pickles"

try:
    MOUNT_PATH = pickle.load(open(f"{PICKLE_PATH}/MOUNT_PATH.pkl", "rb"))
    housing = pickle.load(open(f"{PICKLE_PATH}/housing.pkl", "rb"))
    train_set = pickle.load(open(f"{PICKLE_PATH}/train_set.pkl", "rb"))
    test_set = pickle.load(open(f"{PICKLE_PATH}/test_set.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print("failed to load pickle, ignored")
    print(str(e))

## $xpr_param_component_name = data_versioning_random
## $xpr_param_component_type = pipeline_job
## $xpr_param_global_variables = ["MOUNT_PATH","housing","train_set", "test_set"]

housing["income_cat"] = pd.cut(housing["median_income"],
                               bins=[0., 1.5, 3.0, 4.5, 6., np.inf],
                               labels=[1, 2, 3, 4, 5])
housing["income_cat"].value_counts()
housing["income_cat"].hist()
plt.title("Median Income Distribution")
train_set, test_set = train_test_split(housing, test_size=0.2, random_state=42)
test_set["income_cat"].value_counts() / len(test_set)
train_set["income_cat"].value_counts() / len(train_set)
housing["income_cat"].value_counts() / len(housing)
for dataset in [train_set, test_set]:
    dataset.drop( axis=1, columns="income_cat", inplace=True)


PICKLE_PATH = "/mnt/nfs/data/jupyter_experiments/projects/testing_house_5/pickles"

try:
    pickle.dump(MOUNT_PATH, open(f"{PICKLE_PATH}/MOUNT_PATH.pkl", "wb"))
    pickle.dump(housing, open(f"{PICKLE_PATH}/housing.pkl", "wb"))
    pickle.dump(train_set, open(f"{PICKLE_PATH}/train_set.pkl", "wb"))
    pickle.dump(test_set, open(f"{PICKLE_PATH}/test_set.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
