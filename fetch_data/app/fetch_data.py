MOUNT_PATH = None
import os
import sys
import types
import pickle
import urllib
import marshal
import sklearn
import tarfile
import warnings
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer
from pandas.plotting import scatter_matrix
from sklearn.compose import ColumnTransformer
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
from sklearn.model_selection import StratifiedShuffleSplit
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot

PICKLE_PATH = "/mnt/nfs/data/jupyter_experiments/projects/testing_house_5/pickles"

try:
    MOUNT_PATH = pickle.load(open(f"{PICKLE_PATH}/MOUNT_PATH.pkl", "rb"))
    housing = pickle.load(open(f"{PICKLE_PATH}/housing.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print("failed to load pickle, ignored")
    print(str(e))


PICKLE_PATH = "/mnt/nfs/data/jupyter_experiments/projects/testing_house_5/pickles"

try:
    model_evaluation = types.FunctionType(marshal.loads(pickle.load(open(f"{PICKLE_PATH}/model_evaluation.pkl", "rb"))), {})
except (NameError, FileNotFoundError) as e:
    print("failed to load pickle, ignored")
    print(str(e))

# Setup components and global variables for Xpresso to recognize
## $xpr_param_component_name = fetch_data
## $xpr_param_component_type = pipeline_job 
## $xpr_param_global_variables = ["MOUNT_PATH","housing"]
## $xpr_param_global_methods =["model_evaluation"]


# Define default file path for Xpresso to save plots
MOUNT_PATH = "/data"

assert sys.version_info >= (3, 5)



# Ignore useless warnings (see SciPy issue #5998)
warnings.filterwarnings(action="ignore", message="^internal gelsd")


# To plot pretty figures
#%matplotlib inline

mpl.rc('axes', labelsize=14)
mpl.rc('xtick', labelsize=12)
mpl.rc('ytick', labelsize=12)

    
# define method for model evaluation
def model_evaluation(model, X_train, X_test, y_train, y_test):
    # Apply trained model to predict the test set
    y_pred_train = model.predict(X_train)

    mse_train = mean_squared_error(y_train, y_pred_train)

    rmse_train = np.sqrt(mse_train)

    mae_train = mean_absolute_error(y_train, y_pred_train)


    # Apply trained model to predict the train set
    y_pred_test = model.predict(X_test)

    mse_test = mean_squared_error(y_test, y_pred_test)

    rmse_test = np.sqrt(mse_test)

    mae_test = mean_absolute_error(y_test, y_pred_test)

    model_scores = pd.DataFrame({"Metrics":["MSE","RMSE", "MAE" ],
                "Train set":np.round([mse_train,rmse_train, mae_train ],2),
                "Test set":np.round([mse_test,rmse_test, mae_test ],2)})
    return model_scores



DOWNLOAD_ROOT = "https://raw.githubusercontent.com/ageron/handson-ml2/master/"
HOUSING_PATH = os.path.join("datasets", "housing")
HOUSING_URL = DOWNLOAD_ROOT + "datasets/housing/housing.tgz"

def fetch_housing_data(housing_url=HOUSING_URL, housing_path=HOUSING_PATH):
    if not os.path.isdir(housing_path):
        os.makedirs(housing_path)
    tgz_path = os.path.join(housing_path, "housing.tgz")
    urllib.request.urlretrieve(housing_url, tgz_path)
    housing_tgz = tarfile.open(tgz_path)
    housing_tgz.extractall(path=housing_path)
    housing_tgz.close()
# Fetch dataset and extract the zip file
fetch_housing_data()

def load_housing_data(housing_path=HOUSING_PATH):
    csv_path = os.path.join(housing_path, "housing.csv")
    return pd.read_csv(csv_path)
# Load dataset into csv file
housing = load_housing_data()
housing.head()
housing.info()
housing["ocean_proximity"].value_counts()
housing.describe()
#%matplotlib inline

housing.hist(bins=50, figsize=(20,15))
xpresso_save_plot("image_1", output_path=MOUNT_PATH,output_folder="testing_house_5/fetch_data")


PICKLE_PATH = "/mnt/nfs/data/jupyter_experiments/projects/testing_house_5/pickles"

try:
    pickle.dump(marshal.dumps(model_evaluation.__code__), open(f"{PICKLE_PATH}/model_evaluation.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))


PICKLE_PATH = "/mnt/nfs/data/jupyter_experiments/projects/testing_house_5/pickles"

try:
    pickle.dump(MOUNT_PATH, open(f"{PICKLE_PATH}/MOUNT_PATH.pkl", "wb"))
    pickle.dump(housing, open(f"{PICKLE_PATH}/housing.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
