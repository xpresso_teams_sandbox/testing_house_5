MOUNT_PATH = None
import os
import sys
import types
import pickle
import urllib
import marshal
import sklearn
import tarfile
import warnings
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer
from pandas.plotting import scatter_matrix
from sklearn.compose import ColumnTransformer
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
from sklearn.model_selection import StratifiedShuffleSplit
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot

PICKLE_PATH = "/mnt/nfs/data/jupyter_experiments/projects/testing_house_5/pickles"

try:
    MOUNT_PATH = pickle.load(open(f"{PICKLE_PATH}/MOUNT_PATH.pkl", "rb"))
    housing = pickle.load(open(f"{PICKLE_PATH}/housing.pkl", "rb"))
    train_set = pickle.load(open(f"{PICKLE_PATH}/train_set.pkl", "rb"))
    test_set = pickle.load(open(f"{PICKLE_PATH}/test_set.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print("failed to load pickle, ignored")
    print(str(e))

## $xpr_param_component_name = explore_data
## $xpr_param_component_type = pipeline_job
## $xpr_param_global_variables = ["MOUNT_PATH","housing", "train_set", "test_set"]


# inspect housing data on a map
train_set.plot(kind="scatter", x="longitude", y="latitude", alpha=0.1)
plt.title("Housing data on a map")
# housing prices spread on a map
train_set.plot(kind="scatter", x="longitude", y="latitude", alpha=0.4,
    s=train_set["population"]/100, label="population", figsize=(10,7),
    c="median_house_value", cmap=plt.get_cmap("jet"), colorbar=True,
    sharex=False)
plt.legend()
plt.title("housing_prices_scatterplot")
# Download the California image
images_path = os.path.join(PROJECT_ROOT_DIR, "images", "end_to_end_project")
os.makedirs(images_path, exist_ok=True)
DOWNLOAD_ROOT = "https://raw.githubusercontent.com/ageron/handson-ml2/master/"
filename = "california.png"
print("Downloading", filename)
url = DOWNLOAD_ROOT + "images/end_to_end_project/" + filename
urllib.request.urlretrieve(url, os.path.join(images_path, filename))
california_img=mpimg.imread(os.path.join(images_path, filename))
ax = train_set.plot(kind="scatter", x="longitude", y="latitude", figsize=(10,7),
                       s=train_set['population']/100, label="Population",
                       c="median_house_value", cmap=plt.get_cmap("jet"),
                       colorbar=False, alpha=0.4,
                      )
plt.imshow(california_img, extent=[-124.55, -113.80, 32.45, 42.05], alpha=0.5,
           cmap=plt.get_cmap("jet"))
plt.ylabel("Latitude", fontsize=14)
plt.xlabel("Longitude", fontsize=14)

prices = train_set["median_house_value"]
tick_values = np.linspace(prices.min(), prices.max(), 11)
cbar = plt.colorbar()
cbar.ax.set_yticklabels(["$%dk"%(round(v/1000)) for v in tick_values], fontsize=14)
cbar.set_label('Median House Value', fontsize=16)

plt.legend(fontsize=16)
plt.title("california_housing_prices_plot")
xpresso_save_plot("image_1", output_path=MOUNT_PATH,output_folder="testing_house_5/explore_data")
corr_matrix = train_set.corr()
corr_matrix["median_house_value"].sort_values(ascending=False)

attributes = ["median_house_value", "median_income", "total_rooms",
              "housing_median_age"]
scatter_matrix(train_set[attributes], figsize=(12, 8))
plt.title("scatter_matrix_plot")
train_set.plot(kind="scatter", x="median_income", y="median_house_value",
             alpha=0.1)
plt.axis([0, 16, 0, 550000])
plt.title("income_vs_house_value_scatterplot")
train_set["rooms_per_household"] = train_set["total_rooms"]/train_set["households"]
train_set["bedrooms_per_room"] = train_set["total_bedrooms"]/train_set["total_rooms"]
train_set["population_per_household"]=train_set["population"]/train_set["households"]
corr_matrix = train_set.corr()
corr_matrix["median_house_value"].sort_values(ascending=False)
train_set.plot(kind="scatter", x="rooms_per_household", y="median_house_value",
             alpha=0.2)
plt.axis([0, 5, 0, 520000])
xpresso_save_plot("image_2", output_path=MOUNT_PATH,output_folder="testing_house_5/explore_data")


PICKLE_PATH = "/mnt/nfs/data/jupyter_experiments/projects/testing_house_5/pickles"

try:
    pickle.dump(MOUNT_PATH, open(f"{PICKLE_PATH}/MOUNT_PATH.pkl", "wb"))
    pickle.dump(housing, open(f"{PICKLE_PATH}/housing.pkl", "wb"))
    pickle.dump(train_set, open(f"{PICKLE_PATH}/train_set.pkl", "wb"))
    pickle.dump(test_set, open(f"{PICKLE_PATH}/test_set.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
