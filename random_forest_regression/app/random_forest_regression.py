MOUNT_PATH = None
import os
import sys
import types
import pickle
import urllib
import marshal
import sklearn
import tarfile
import warnings
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer
from pandas.plotting import scatter_matrix
from sklearn.compose import ColumnTransformer
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
from sklearn.model_selection import StratifiedShuffleSplit
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot

PICKLE_PATH = "/mnt/nfs/data/jupyter_experiments/projects/testing_house_5/pickles"

try:
    MOUNT_PATH = pickle.load(open(f"{PICKLE_PATH}/MOUNT_PATH.pkl", "rb"))
    train_x_trans = pickle.load(open(f"{PICKLE_PATH}/train_x_trans.pkl", "rb"))
    labels_train = pickle.load(open(f"{PICKLE_PATH}/labels_train.pkl", "rb"))
    test_x_trans = pickle.load(open(f"{PICKLE_PATH}/test_x_trans.pkl", "rb"))
    labels_test = pickle.load(open(f"{PICKLE_PATH}/labels_test.pkl", "rb"))
    model = pickle.load(open(f"{PICKLE_PATH}/model.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print("failed to load pickle, ignored")
    print(str(e))


PICKLE_PATH = "/mnt/nfs/data/jupyter_experiments/projects/testing_house_5/pickles"

try:
    model_evaluation = types.FunctionType(marshal.loads(pickle.load(open(f"{PICKLE_PATH}/model_evaluation.pkl", "rb"))), {})
except (NameError, FileNotFoundError) as e:
    print("failed to load pickle, ignored")
    print(str(e))

## $xpr_param_component_name = random_forest_regression
## $xpr_param_component_type = pipeline_job
## $xpr_param_global_variables = ["MOUNT_PATH", "train_x_trans", "labels_train", "test_x_trans", "labels_test","model"]
## $xpr_param_global_methods =["model_evaluation"]



model = RandomForestRegressor(n_estimators=10, random_state=42)
model.fit(train_x_trans, labels_train)
model_metrics = model_evaluation(model,train_x_trans,test_x_trans, labels_train,  labels_test )
print(str(model))
print(model_metrics)


PICKLE_PATH = "/mnt/nfs/data/jupyter_experiments/projects/testing_house_5/pickles"

try:
    pickle.dump(marshal.dumps(model_evaluation.__code__), open(f"{PICKLE_PATH}/model_evaluation.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))


PICKLE_PATH = "/mnt/nfs/data/jupyter_experiments/projects/testing_house_5/pickles"

try:
    pickle.dump(MOUNT_PATH, open(f"{PICKLE_PATH}/MOUNT_PATH.pkl", "wb"))
    pickle.dump(train_x_trans, open(f"{PICKLE_PATH}/train_x_trans.pkl", "wb"))
    pickle.dump(labels_train, open(f"{PICKLE_PATH}/labels_train.pkl", "wb"))
    pickle.dump(test_x_trans, open(f"{PICKLE_PATH}/test_x_trans.pkl", "wb"))
    pickle.dump(labels_test, open(f"{PICKLE_PATH}/labels_test.pkl", "wb"))
    pickle.dump(model, open(f"{PICKLE_PATH}/model.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
