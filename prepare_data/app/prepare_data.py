MOUNT_PATH = None
import os
import sys
import types
import pickle
import urllib
import marshal
import sklearn
import tarfile
import warnings
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer
from pandas.plotting import scatter_matrix
from sklearn.compose import ColumnTransformer
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
from sklearn.model_selection import StratifiedShuffleSplit
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot

PICKLE_PATH = "/mnt/nfs/data/jupyter_experiments/projects/testing_house_5/pickles"

try:
    MOUNT_PATH = pickle.load(open(f"{PICKLE_PATH}/MOUNT_PATH.pkl", "rb"))
    housing = pickle.load(open(f"{PICKLE_PATH}/housing.pkl", "rb"))
    train_set = pickle.load(open(f"{PICKLE_PATH}/train_set.pkl", "rb"))
    test_set = pickle.load(open(f"{PICKLE_PATH}/test_set.pkl", "rb"))
    train_x_trans = pickle.load(open(f"{PICKLE_PATH}/train_x_trans.pkl", "rb"))
    labels_train = pickle.load(open(f"{PICKLE_PATH}/labels_train.pkl", "rb"))
    test_x_trans = pickle.load(open(f"{PICKLE_PATH}/test_x_trans.pkl", "rb"))
    labels_test = pickle.load(open(f"{PICKLE_PATH}/labels_test.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print("failed to load pickle, ignored")
    print(str(e))

## $xpr_param_component_name = prepare_data	
## $xpr_param_component_type = pipeline_job
## $xpr_param_global_variables = ["MOUNT_PATH","housing","train_set", "test_set","train_x_trans", "labels_train", "test_x_trans", "labels_test"]

train_x = train_set.drop("median_house_value", axis=1) # drop labels for training set
labels_train = train_set["median_house_value"].copy()
imputer = SimpleImputer(strategy="median")
#housing_num = train_x.drop("ocean_proximity", axis=1)
# alternatively: 
train_x_num = train_x.select_dtypes(include=[np.number])
train_x_num.head()
imputer.fit(train_x_num)
housing_cat= [col for col in train_x.columns if col not in train_x_num]
housing_cat

cat_encoder = OneHotEncoder()
housing_cat_1hot = cat_encoder.fit_transform(train_x[housing_cat])

#housing_cat_1hot.toarray()
cat_encoder = OneHotEncoder(sparse=False)
housing_cat_1hot = cat_encoder.fit_transform(train_x[housing_cat])


#cat_encoder.categories_

imputer = SimpleImputer(strategy="median")

num_pipeline = Pipeline([
        ('imputer', SimpleImputer(strategy="median")),
        ('std_scaler', StandardScaler()),
    ])

housing_num_tr = num_pipeline.fit_transform(train_x_num)
#housing_num_tr

num_attribs = train_x.select_dtypes(include=[np.number]).columns
cat_attribs = [col for col in train_x.columns if col not in num_attribs]

full_pipeline = ColumnTransformer([
        ("num", num_pipeline, num_attribs),
        ("cat", OneHotEncoder(), cat_attribs),
    ])

train_x_trans = full_pipeline.fit_transform(train_x)
# create additional features
test_set["rooms_per_household"] = test_set["total_rooms"]/test_set["households"]
test_set["bedrooms_per_room"] = test_set["total_bedrooms"]/test_set["total_rooms"]
test_set["population_per_household"]=test_set["population"]/test_set["households"]
# create test_x and test labels
test_x = test_set.drop("median_house_value", axis=1) # drop labels for test set
labels_test = test_set["median_house_value"].copy()
#transform test set using the pipeline trained on training set
test_x_trans = full_pipeline.transform(test_x)


PICKLE_PATH = "/mnt/nfs/data/jupyter_experiments/projects/testing_house_5/pickles"

try:
    pickle.dump(MOUNT_PATH, open(f"{PICKLE_PATH}/MOUNT_PATH.pkl", "wb"))
    pickle.dump(housing, open(f"{PICKLE_PATH}/housing.pkl", "wb"))
    pickle.dump(train_set, open(f"{PICKLE_PATH}/train_set.pkl", "wb"))
    pickle.dump(test_set, open(f"{PICKLE_PATH}/test_set.pkl", "wb"))
    pickle.dump(train_x_trans, open(f"{PICKLE_PATH}/train_x_trans.pkl", "wb"))
    pickle.dump(labels_train, open(f"{PICKLE_PATH}/labels_train.pkl", "wb"))
    pickle.dump(test_x_trans, open(f"{PICKLE_PATH}/test_x_trans.pkl", "wb"))
    pickle.dump(labels_test, open(f"{PICKLE_PATH}/labels_test.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
